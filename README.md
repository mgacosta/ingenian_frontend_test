**Ingenian Software Frontend Test**

This project was created with django and pure javascript.

When you going to http://localhost:8000/quotes/ you get the free Quote of the day from https://quotes.rest/#!/qod/get_qod​.

The page loads the title, quote, author and the page name to accomplish with copyright laws.

And finally, it's necessary to clarify how i understand a fetch() javascript's function and the SAP concept.
I think, this function actually comply with the SAP architecture, because:

1. It's called by demand into the mains functions (i guess, could better if the script was into a separate file), and
2. Because only update the necessary code into html page.

## About Project:

1. test_project => It's the main django's project
2. quotes => It's the current application thats request for the quotes and render the response

## Dependencies:

1. Django: 2.0.5
2. Python: 3.6.4
