from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    #return HttpResponse("First View for Quotes")
    return render(request, 'quotes/index.html')
